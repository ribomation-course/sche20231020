# Installation Instructions


# GIT
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/jenkins-course/my-solutions
    cd ~/jenkins-course
    git clone <https url to this repo> gitlab

![GIT HTTPS URL](./img/git-url.png)

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/angular-course/gitlab
    git pull

# SaaS Accounts

## GitHub
Create an account at GitHub, unless you already have one.
* https://github.com/


## Google Cloud
Create a Google account, unless you laready have it and then
sign up for Google Cloud Platform (GCP)

* https://cloud.google.com/


# Ubuntu @ WSL2 @ Windows 11
Unless you already have it, configure and install Ubuntu running on WSL2.

* https://learn.microsoft.com/en-us/windows/wsl/install

Then, install Ubuntu from Microsoft Store. Just open the app
and search for Ubuntu. Choose the latest version.

Within Ubuntu, install

* Installera SDKMAN: https://sdkman.io/
* Installera Java JDK 17: sdk install java 17.0.8-oracle
* Installera Jenkins: https://pkg.jenkins.io/debian-stable/

# Zoom
Install the Zoom client.

* [Zoom Download Center](https://zoom.us/download#client_4meeting)
* [Getting started with the Zoom web client](https://support.zoom.us/hc/en-us/articles/214629443-Getting-started-with-the-Zoom-web-client)
* [Getting started on Windows](https://support.zoom.us/hc/en-us/articles/201362033-Getting-started-on-Windows-and-macOS)
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)




