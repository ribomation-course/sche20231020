# Jenkins CI Server, 2023 October

Welcome to this course
* [Course](https://www.ribomation.se/programmerings-kurser/dev-tools/jenkins/)
* [Installation Instructions](./installation-instructions.md)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
